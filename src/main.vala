///
/// Program-Name:	Random Task
/// Description: 	Outputs a random line of one of five files, 
///					selected by a buttonpress of the header
/// Author: 		ConfusedAnt (Yanik Ammann)
///	Last Changed: 	13.11.2019 by ConfusedAnt
///	Prerequisites: 	none
/// Build-command:	valac --pkg gtk+-3.0 main.vala; ./main
///
/// To-Do:			- Vertically center text
///                 - make icons available if no icon-theme that supports them is found
///
///	Versions (version-number, date, author, description):
/// v0.0, 29.10.2019, ConfusedAnt, starting developement
/// v0.1, 13.11.2019, ConfusedAnt, added buttonfunctionality + can now read files + added comments 
/// v0.2, 13.11.2019, ConfusedAnt, added file-header + fixed formating
/// v0.3, 14.11.2019, ConfusedAnt, added history buttons (at the end of header) + add history functionality + improve icons
/// v0.4, 14.11.2019, ConfusedAnt, added open functionality for history + added demo files
/// v0.5, 14.11.2019, ConfusedAnt, added random button and "number"-file
/// v0.6, 16.11.2019, ConfusedAnt, minor bug-fixes and tweaks
/// v0.7, 21.11.2019, ConfusedAnt, changed icons... again
///


using Gtk;

class RandomBoyTask : Window {
  TextView text_view;
  string history = "";
  string[] numberStrings = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", 
                             "15", "16", "17", "18", "19", "20", "21", "22", "23", "24" };

  RandomBoyTask () {
        // set header parameters
        var header = new HeaderBar ();
        header.title = "Random Task Generator";
        header.show_close_button = true;
        header.spacing = 0;
	    // set window parameters
        window_position = WindowPosition.CENTER;
        set_titlebar(header);
        set_default_size (550, 300);
        border_width = 2;

	    // define icons
        var random_icon = new Image.from_icon_name ("amarok_lyrics", IconSize.SMALL_TOOLBAR);
        var shuffle_icon = new Image.from_icon_name ("arrow-right", IconSize.SMALL_TOOLBAR); //gtk-refresh
        var minus_icon = new Image.from_icon_name ("arrow-down", IconSize.SMALL_TOOLBAR);
        var plus_icon = new Image.from_icon_name ("arrow-up", IconSize.SMALL_TOOLBAR);
        var people_icon = new Image.from_icon_name ("tag-people", IconSize.SMALL_TOOLBAR); //resource-group
        var release_icon = new Image.from_icon_name ("arrow-left-double", IconSize.SMALL_TOOLBAR); //application-exit
        var number_icon = new Image.from_icon_name ("raindrop", IconSize.SMALL_TOOLBAR);
        
        var add_history_icon = new Image.from_icon_name ("bookmark_add", IconSize.SMALL_TOOLBAR);
        var open_history_icon = new Image.from_icon_name ("bookmarks", IconSize.SMALL_TOOLBAR);

	    // random
	    var random_button = new ToolButton (random_icon, "Random");
        random_button.is_important = true;
        random_button.clicked.connect (on_random_clicked);
        // task
	    var shuffle_button = new ToolButton (shuffle_icon, "Task");
        shuffle_button.is_important = true;
        shuffle_button.clicked.connect (on_shuffle_clicked);
        // negative
        var minus_button = new ToolButton (minus_icon, "Minus");
        minus_button.is_important = true;
        minus_button.clicked.connect (on_negative_clicked);
	    // positive
        var plus_button = new ToolButton (plus_icon, "Plus");
        plus_button.is_important = true;
        plus_button.clicked.connect (one_positive_clicked);
        // public
        var public_button = new ToolButton (people_icon, "Public");
        public_button.is_important = true;
        public_button.clicked.connect (on_public_clicked);
        // release
        var release_button = new ToolButton (release_icon, "Release");
        release_button.is_important = true;
        release_button.clicked.connect (on_release_clicked);
        // number
        var number_button = new ToolButton (number_icon, "Number");
        number_button.is_important = true;
        number_button.clicked.connect (on_number_clicked);
        
        // add to history
        var add_history_button = new ToolButton (add_history_icon, "History");
        add_history_button.is_important = true;
        add_history_button.clicked.connect (on_add_history_clicked);
        // open history
        var open_history_button = new ToolButton (open_history_icon, "Release");
        open_history_button.is_important = true;
        open_history_button.clicked.connect (on_show_history_clicked);

	    // add items to header		
        //header.add(random_button);
        header.add(shuffle_button);
        header.add(minus_button);
        header.add(plus_button);
        header.add(public_button);
        header.add(release_button);
        header.add(number_button);
        // add to end of header  
        header.pack_end(open_history_button);
        header.pack_end(add_history_button);
        header.pack_end(random_button);
        
        // add textView
        text_view = new TextView ();
        text_view.editable = false;
        text_view.cursor_visible = false;
        text_view.wrap_mode = WORD;
        text_view.justification = CENTER;

	    // create window that's scrollable and has textView in it
        var scroll = new ScrolledWindow (null, null);
        scroll.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        scroll.add (text_view);
        var vbox = new Box (Orientation.VERTICAL, 0);
        vbox.pack_start (scroll, true, true, 0);
        add (vbox);
 	}
    
    // random
    void on_random_clicked () {
        string[] availableFiles = { "tasks.txt", "negative.txt", "positive.txt", "public.txt", "release.txt", "number.txt" };
        string intputText = "";
        foreach (string filename in availableFiles) {
            // open file for reading
		    FileStream stream = FileStream.open (filename, "r");
		    assert (stream != null);

		    // buffered:
		    char buf[1024];
		    while (stream.gets (buf) != null) {
			    intputText += ((string) buf);
		    }
        }
	
		// randomly select one entry
		string[] output = intputText.split("\n");
		int i = Random.int_range(0, output.length);
		// check that that entry is not empty
		while (output[i] == "" || output[i] == " " || output[i] == "\n") {
			i = Random.int_range(0, output.length);
		}
		this.text_view.buffer.text = "\n" + output[i];
	}
	
    // tasks
    void on_shuffle_clicked () {
		 this.text_view.buffer.text = readFile("tasks.txt");
	}
	// negative
	void on_negative_clicked () {
		this.text_view.buffer.text = readFile("negative.txt");
	}
	// positive
	void one_positive_clicked () {
		this.text_view.buffer.text = readFile("positive.txt");
	}
	// public
	void on_public_clicked () {
		this.text_view.buffer.text = readFile("public.txt");
	}
	// release
	void on_release_clicked () {
		this.text_view.buffer.text = readFile("release.txt");
	}
	// number
	void on_number_clicked () {
		this.text_view.buffer.text = readFile("number.txt");
	}
	
	
	// public
	void on_add_history_clicked () {
		history += text_view.buffer.text;
	}
	// release
	void on_show_history_clicked () {
		this.text_view.buffer.text = history;
        try {
            string output = "index.php?";
            string[] historyArray = history.replace(" ", "+").split("\n");
            for (int i = 1; i < historyArray.length + 1; i++) {
                output += "c" + numberStrings[i] + "=" + historyArray[i] + "&";
            }
            output += "time=6";
		    Process.spawn_command_line_async ("firefox https://wheeldecide.com/" + output); // c1=123&c2=456&c3=789&time=5
	    } catch (SpawnError e) {
		    print ("Error: %s\n", e.message);
	    }
	}
	 
 	string readFile(string filename) {
 		// open file for reading
		FileStream stream = FileStream.open (filename, "r");
		assert (stream != null);

		// buffered:
		char buf[1024];
		string intputText = "";
		while (stream.gets (buf) != null) {
			intputText += ((string) buf);
		}
		
		// randomly select one entry
		string[] output = intputText.split("\n");
		int i = Random.int_range(0, output.length);
		// check that that entry is not empty
		while (output[i] == "" || output[i] == " " || output[i] == "\n") {
			i = Random.int_range(0, output.length);
		}
		return "\n" + output[i];
	}
 	 
	static int main (string[] args) {
		init (ref args);

		// create window
		var window = new RandomBoyTask();
		window.destroy.connect(main_quit);
		window.show_all();

		Gtk.main ();
		return 0;
  }
}